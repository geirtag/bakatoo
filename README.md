1) laadige alla IntelliJ Ultimate
    https://www.jetbrains.com/idea/download/#section=windows
    
    
2) laadige alla MariaDB ja installige
    https://downloads.mariadb.org/mariadb/10.2.15/
    
    
3) avage tekkinud client nimega "MySQL Client (MariaDB 10.2)"


4) kirjutage terminali "mysql -u root -p"


5) sisestage parool, mille panite loodud MariaDB kontole


6) looge andmebaas "CREATE DATABASE bakadb;"


7) kloonige repositoorium arvutisse aadressilt:
    https://bitbucket.org/geirtag/bakatoo/src/master/
    
    
8) tehke IntelliJ-s uus projekt.
    File -> New -> Project from existing sources -> bakatoo (selle kausta sees peaksid olema src folder ja muu).
    Lisage linnuke "Import project from external model" ning valige "Gradle". Vajutage "Next". Pange linnuke "Use auto-import".
    
    
8) lisage projektile Spring Boot konfiguratsioon.
    näide: https://i.gyazo.com/2c3bc4375f52291a6f34f2f9a4d582de.png
    
    
9) Lisage projektile andmebaas.
    IntelliJ aknas New -> Data Source -> MySQL
    Näide: 
    https://i.gyazo.com/116e8e603bf225ce64add04156cfc0c7.png
    
    
10) Vajutage CTRL + SHIFT + A,  otsige "Annotation Processors"


11) Pange linnuke "Enable annotation processing"


12) Käivitage rakendus

