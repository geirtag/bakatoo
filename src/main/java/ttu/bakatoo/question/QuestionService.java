package ttu.bakatoo.question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ttu.bakatoo.test.Test;
import ttu.bakatoo.test.TestName;
import ttu.bakatoo.test.TestRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    TestRepository testRepository;

    public List<Question> findRandomTen(TestName name) {

        Test test = testRepository.findByName(name);
        List<Question> questions = test.getQuestions();
        List<Question> tenRandomQuestions = new ArrayList<>();

        while (tenRandomQuestions.size() < 10) {
            int rand = new Random().nextInt(questions.size());
            if (!tenRandomQuestions.contains(questions.get(rand))) {
                tenRandomQuestions.add(questions.get(rand));
            }
        }
        return tenRandomQuestions;
    }

    public Question findOne(long id) {
        return questionRepository.findOne(id);
    }
}
