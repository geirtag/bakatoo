package ttu.bakatoo.submittedtest;

import org.springframework.data.repository.CrudRepository;
import ttu.bakatoo.user.User;

import java.util.List;

public interface SubmittedTestRepository extends CrudRepository<SubmittedTest, Long> {

    List<SubmittedTest> findAllByUser(User user);

}
