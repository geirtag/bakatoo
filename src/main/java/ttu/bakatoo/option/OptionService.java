package ttu.bakatoo.option;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OptionService {

    @Autowired
    OptionRepository optionRepository;

    public Iterable<Options> findAll() {
        return optionRepository.findAll();
    }

    public Options findOne(long id) {
        return optionRepository.findOne(id);
    }
}
