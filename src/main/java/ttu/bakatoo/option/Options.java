package ttu.bakatoo.option;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Options {

    @Id
    @GeneratedValue
    private long id;
    private String text;
    private boolean isCorrect;

    public Options(String text) {
        this.text = text;
    }


    public Options(String text, boolean isCorrect) {
        this.text = text;
        this.isCorrect = isCorrect;
    }


}
