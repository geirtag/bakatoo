package ttu.bakatoo.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


//suhtleb andmebaasiga
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByFacebookId(String facebookId);
}
