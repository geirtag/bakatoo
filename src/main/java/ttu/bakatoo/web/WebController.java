package ttu.bakatoo.web;

import com.sun.java.swing.plaf.windows.WindowsTreeUI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ttu.bakatoo.option.OptionRepository;
import ttu.bakatoo.option.Options;
import ttu.bakatoo.question.Question;
import ttu.bakatoo.question.QuestionService;
import ttu.bakatoo.submittedanswer.SubmittedAnswer;
import ttu.bakatoo.submittedanswer.SubmittedAnswerRepository;
import ttu.bakatoo.submittedquestion.SubmittedQuestion;
import ttu.bakatoo.submittedquestion.SubmittedQuestionRepository;
import ttu.bakatoo.submittedtest.SubmittedTest;
import ttu.bakatoo.submittedtest.SubmittedTestRepository;
import ttu.bakatoo.test.TestName;
import ttu.bakatoo.test.TestService;
import ttu.bakatoo.test.helpers.TestFormHelper;
import ttu.bakatoo.test.helpers.TestFormQuestionHelper;
import ttu.bakatoo.user.User;
import ttu.bakatoo.user.UserService;

import java.security.Principal;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class WebController {

    @Autowired
    TestService testService;

    @Autowired
    QuestionService questionService;

    @Autowired
    OptionRepository optionRepository;

    @Autowired
    SubmittedAnswerRepository submittedAnswerRepository;

    @Autowired
    SubmittedQuestionRepository submittedQuestionRepository;

    @Autowired
    SubmittedTestRepository submittedTestRepository;

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView landingPageView(Principal principal) {
        if (principal == null) {
            return new ModelAndView("login");
        }
        return authenticatedView();
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView authenticatedView() {
        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/home");

        //annan objektile nime, ja testService.findAll leiab objekti
        view.addObject("tests", testService.findAll());
        return view;
    }


    @RequestMapping(value = "/firearmsTesting", method = RequestMethod.GET)
    public ModelAndView firearmsTestsView() {
        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/firearmsTesting");
        return view;
    }

    @RequestMapping(value = "/firearmsTest", method = RequestMethod.GET)
    public ModelAndView firearmsTest() {

        List<Question> randomQuestions = questionService.findRandomTen(TestName.RELVAEKSAM);

        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/firearmsTesting");
        view.addObject("isTest", true);
        view.addObject("testFormHelper",
                new TestFormHelper(testService.findByName(TestName.RELVAEKSAM).getId(), randomQuestions));

        return view;
    }

    @RequestMapping(value = "/submitFirearmsTest", method = RequestMethod.POST)
    public ModelAndView submitFirearmsTest(@ModelAttribute TestFormHelper testFormHelper,   // ülevalt meetodist tulenev testformhelper
                                           Model model,
                                           Authentication authentication) {

        User user = userService.findByFacebookId((String)authentication.getPrincipal()).get();
        List<SubmittedAnswer> submittedAnswers = testFormHelper.getQuestions().stream()
                .map(TestFormQuestionHelper::getOptions)  // 10  küsimuste listi kus sees 4 optioniga listid
                .flatMap(Collection::stream) // panen eelnevad kokku 40-seks listiks
                .map(formOption -> {
                    Options actualOption = optionRepository.findOne(formOption.getOptionId());
                    SubmittedAnswer submittedAnswer = new SubmittedAnswer();
                    submittedAnswer.setOption(actualOption);
                    submittedAnswer.setGuess(formOption.isGuess());
                    submittedAnswer.setGuessIsCorrect(actualOption.isCorrect() == submittedAnswer.isGuess());
                    return submittedAnswer; // 40 optionist loob 40 submitted answeri
                })
                .map(submittedAnswer -> submittedAnswerRepository.save(submittedAnswer))
                .collect(Collectors.toList());   // panen listi

        List<SubmittedQuestion> submittedQuestions = testFormHelper.getQuestions().stream()
                .map(formQuestion -> {
                    Question actualQuestion = questionService.findOne(formQuestion.getQuestionId());
                    List<SubmittedAnswer> actualQuestionAnswers = submittedAnswers.stream()
                            .filter(submittedAnswer -> actualQuestion.getOptions().contains(submittedAnswer.getOption())) // filtreerid v2lja need neli vastust, mis on seotud ainult selle kysimusega
                            .collect(Collectors.toList());
                    return new SubmittedQuestion(actualQuestion, actualQuestionAnswers);
                })
                .map(submittedQuestion -> submittedQuestionRepository.save(submittedQuestion))
                .collect(Collectors.toList());

        SubmittedTest submittedTest = submittedTestRepository.save(new SubmittedTest(user, testService.findOne(testFormHelper.getTestId()), submittedQuestions));

        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/results");
        view.addObject("submittedTest", submittedTest);
        return view;
    }

    @RequestMapping(value = "/trafficTests", method = RequestMethod.GET)
    public ModelAndView trafficTestsView() {
        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/trafficTests");
        return view;
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public ModelAndView resultsView(Authentication authentication) {
        User user = userService.findByFacebookId((String) authentication.getPrincipal()).get();
        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/results");
        view.addObject("showList", true);
        view.addObject("submittedTests", submittedTestRepository.findAllByUser(user).stream()
                .sorted(Comparator.comparing(SubmittedTest::getSubmitDateTime).reversed())
                .collect(Collectors.toList()));
        return view;
    }

    @RequestMapping(value = "/results/{submittedTestId}", method = RequestMethod.GET)
    public ModelAndView resultsView(@PathVariable(value = "submittedTestId") long submittedTestId) {
        SubmittedTest submittedTest = submittedTestRepository.findOne(submittedTestId);
        ModelAndView view = new ModelAndView();
        view.setViewName("authenticated/results");
        view.addObject("submittedTest", submittedTest);
        return view;
    }

}
