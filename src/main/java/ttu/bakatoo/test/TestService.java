package ttu.bakatoo.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestService {

    @Autowired
    TestRepository testRepository;

    public List<Test> findAll() {
        return testRepository.findAll();
    }

    public Test findOne(long id) {
        return testRepository.findOne(id);
    }

    public Test findByName(TestName name) { return testRepository.findByName(name); }
}
