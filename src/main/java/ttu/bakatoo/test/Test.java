package ttu.bakatoo.test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.question.Question;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Test {

    @Id
    @GeneratedValue
    private long id;

    @Enumerated(EnumType.STRING)
    private TestName name;

    @OneToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "rel_test_question",
            joinColumns=@JoinColumn(name="test_id", referencedColumnName = "id"),
            inverseJoinColumns=@JoinColumn(name="question_id", referencedColumnName = "id"))
    private List<Question> questions;

    public Test(TestName name, List<Question> questions) {
        this.name = name;
        this.questions = questions;
    }



}
