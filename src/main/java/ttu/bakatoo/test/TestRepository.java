package ttu.bakatoo.test;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestRepository extends CrudRepository<Test, Long> {

    @Override
    List<Test> findAll();

    // nime järgi otsib testobjekti
    Test findByName(TestName name);
}
