package ttu.bakatoo.test.helpers;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.question.Question;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class TestFormQuestionHelper {

    private long questionId;

    private String text;

    private List<TestFormOptionsHelper> options;

    public TestFormQuestionHelper(Question question) {
        this.questionId = question.getId();
        this.text = question.getText();
        this.options = question.getOptions().stream()
                .map(TestFormOptionsHelper::new)
                .collect(Collectors.toList());
    }
}
