package ttu.bakatoo.test.helpers;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ttu.bakatoo.option.Options;

@Getter
@Setter
@NoArgsConstructor
public class TestFormOptionsHelper {

    private long optionId;

    private String text;

    private boolean guess;

    public TestFormOptionsHelper(Options options) {
        this.optionId = options.getId();
        this.text = options.getText();
        this.guess = false;
    }
}
