package ttu.bakatoo.submittedquestion;

import org.springframework.data.repository.CrudRepository;

public interface SubmittedQuestionRepository extends CrudRepository<SubmittedQuestion, Long> {
}
